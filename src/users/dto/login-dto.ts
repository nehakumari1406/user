import {
    IsNotEmpty,
    Length,
    IsString,
   
} from 'class-validator';
import { ApiProperty} from '@nestjs/swagger';

export class LoginDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
     email: string;

    @ApiProperty()
    @IsNotEmpty()
    @Length(6, 35)
    @IsString()
    password: string;


}