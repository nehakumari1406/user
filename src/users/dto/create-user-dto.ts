import { Type } from 'class-transformer';
import {
    IsNotEmpty,
    Length,
    IsString,
   
} from 'class-validator';
import { ApiProperty} from '@nestjs/swagger';

export class company {
    name: string
    catchPhrase: string
    bs: string
}
export class CompanyDTO {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    catchPhrase: string;

    @ApiProperty()
    @IsString()
    bs: string;


}
export class CompanyUpdateDTO {
    @ApiProperty()
    company: CompanyDTO

}

export class geo {
    lat: string
    lng: string

}
export class geoDTO {
    @ApiProperty()
    @IsString()
    lat: string;

    @ApiProperty()
    @IsString()
    lng: string;

}
export class GeoUpdateDTO {
    @ApiProperty()
    geo: geoDTO

}
export class address {
    street: string
    suite: string
    city: string
    zipcode: string

}
export class AddressDTO {
    @ApiProperty()
    @IsString()
    street: string;

    @ApiProperty()
    @IsString()
    suite: string;

    @ApiProperty()
    @IsString()
    city: string;

    @ApiProperty()
    @IsString()
    zipcode: string;

    @ApiProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;


}
export class AddressUpdateDTO {
    @ApiProperty()
    address: AddressDTO;



}
export class CreateuserDto {
    @ApiProperty()
    @IsNotEmpty()
    username: string;

    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @IsString()
    @ApiProperty()
    @IsNotEmpty()
    @Length(6, 35)
    password: string;
    @IsString()
    @ApiProperty()
    @IsNotEmpty()
    @Length(10, 11)
    phone: string;

    @ApiProperty()
    @IsNotEmpty()
    email: string;
    @ApiProperty()
    @IsNotEmpty()
    website: string;


    role: string;
    deliveryManagementBy: string;

    @ApiProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;

    @ApiProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;
}