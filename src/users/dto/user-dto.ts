
import {
    IsNotEmpty,
    Length,
    IsString,IsOptional,IsEmpty,IsEnum
   
} from 'class-validator';
import { UserRoles } from '../../util/app.model';
import { Type } from 'class-transformer';
import { ApiProperty} from '@nestjs/swagger';


export class company {
    name: string
    catchPhrase: string
    bs: string
}
export class CompanyDTO {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    catchPhrase: string;

    @ApiProperty()
    @IsString()
    bs: string;


}
export class CompanyUpdateDTO {
    @ApiProperty()
    company: CompanyDTO

}

export class geo {
    lat: string
    lng: string

}
export class geoDTO {
    @ApiProperty()
    @IsString()
    lat: string;

    @ApiProperty()
    @IsString()
    lng: string;

}
export class GeoUpdateDTO {
    @ApiProperty()
    geo: geoDTO

}

export class address {
    street: string
    suite: string
    city: string
    zipcode: string

}
export class AddressDTO {
    @ApiProperty()
    @IsString()
    street: string;

    @ApiProperty()
    @IsString()
    suite: string;

    @ApiProperty()
    @IsString()
    city: string;

    @ApiProperty()
    @IsString()
    zipcode: string;

    @ApiProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;


}
export class AddressUpdateDTO {
    @ApiProperty()
    address: AddressDTO;



}
export class UsersDTO {
    @IsEmpty()
    _id: string;

    @IsString()
    @IsOptional()
    @ApiProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiProperty()
    username: string;

    @IsString()
    @IsOptional()
    @ApiProperty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @Length(5, 35)
    @ApiProperty()
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    phone: string;

    @IsString()
    @ApiProperty()
    website: string;
    
    @IsNotEmpty()
    @ApiProperty({ enum: Object.keys(UserRoles) })
    @IsEnum(UserRoles, {
        message: 'Role type must be one of these ' + Object.keys(UserRoles),
    })
    role: string;

    @ApiProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;

    @ApiProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;
    status: boolean;


}