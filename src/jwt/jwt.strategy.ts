import {Injectable } from '@nestjs/common';
import { PassportStrategy, AuthGuard } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { createParamDecorator} from '@nestjs/common';
import { UtilService } from '../util//util.service';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(
        private userService: UsersService,
        private utilService: UtilService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: process.env.SECRET,
        })
       if (!process.env.SECRET) console.log("SECRET not set.");
    }

    // validates user token and returns user's information
    async validate(payload:any) {
        const { _id } = payload;
        const userInfo = await this.userService.getUserDeatilForJWTValidate(_id);
        console.log(userInfo);
        if (userInfo && !userInfo.status) this.utilService.unauthorized();
        return userInfo;
    }
    
}

// export class OptionalJwtAuthGuard extends AuthGuard('jwt') {
    
//     // Override handleRequest so it never throws an error
//  handleRequest(err, user, info, context) {
      
//         console.log(user);
//         return user;
//     }

// }

// export const GetUser = createParamDecorator((data, req) => {
//     console.log(req.user);
//     return req.user;
// });

